"use strict";

var _ = require('lodash')
  , Promise = require('bluebird')
  , knex = require('knex');

/**
 * Registers an *Express* middleware that adds a *knex.js* database connection to each request.
 *
 * Example usage in config file:
 *
 * ```js
 * features: [
 *   ...
 *   {
 *     feature: 'database'
 *   },
 *   ...
 * ]
 * ```
 *
 * The database connection is established according to the `config.database` and stored to `req.db`.
 *
 * @param {object} app
 *    express.js Application instance.
 */
module.exports = function (app) {
  var databases = {};

  app.db = function () {
    // Throws if the database configuration is request specific.
    // See ConfigManager.knexConfig for more info.
    return database();
  };

  app.disconnectDb = function () {
    var dbs = databases;
    databases = {};
    return Promise.all(_.map(dbs, function (db) {
      return db.destroy();
    }));
  };

  app.use(function (req, res, next) {
    req.db = database(req);
    next();
  });

  function database(req) {
    var knexConfig = app.configManager.knexConfig(req);
    var dbId = knexConfig.client
      + '_' + knexConfig.connection.host
      + '_' + knexConfig.connection.database;

    if (!databases[dbId]) {
      databases[dbId] = knex(knexConfig);
    }

    return databases[dbId];
  }

  configurePostgres();
};

/**
 * @private
 */
var configurePostgres = _.once(function () {
  var pgTypes = require('pg').types;
  var MaxSafeInteger = Math.pow(2, 53) - 1;

  // Convert big integers to numbers.
  pgTypes.setTypeParser(20, function (val) {
    if (val === null) {
      return null;
    }
    var number = parseInt(val, 10);
    if (number > MaxSafeInteger) {
      throw new Error('node-pg: bigint overflow: ' + number);
    }
    return number;
  });

  // Don't parse timestamps to js Date() objects, but just ISO8601 strings
  pgTypes.setTypeParser(1184, function (val) {
    if (val === null) {
      return null;
    }

    // supports following formats:
    //
    // 2014-09-23 15:29:18.37788+03,
    // 2014-09-23 15:29:18.37788+03
    // 2014-09-23 15:29:18-03
    // 2014-09-23 15:29:18.38+03:30
    // 2014-09-23 15:29:18+00
    // 2014-09-23 15:29:18.38

    var parsedDateTime = val.match(/(\d{4}-\d{2}-\d{2}) (\d{2}:\d{2}:\d{2})(\.(\d+))?(([+-])(\d{2})(:(\d{2}))?)?/);

    // output is
    // [
    //   "2014-09-23 15:29:18.38+03:30",
    //   "2014-09-23",
    //   "15:29:18",
    //   ".38",
    //   "38",
    //   "+03:30",
    //   "+","03",":30","30"
    // ]

    var parsedSubSecond = parsedDateTime[4];
    var milliseconds = ".000";
    if (parsedSubSecond) {
      milliseconds = parseFloat(parsedDateTime[3]).toPrecision(3).substring(1);
    }

    var parsedTimeZone = parsedDateTime[5];
    var isoTimeZone = "Z";
    if (parsedTimeZone) {
      var sign = parsedDateTime[6];
      var hours = parsedDateTime[7];
      var minutes = parsedDateTime[9] || "00";
      isoTimeZone = sign + hours + ":" + minutes;
    }

    return parsedDateTime[1] + "T" + parsedDateTime[2] + milliseconds + isoTimeZone;
  });

  // Don't convert json into object. Return as string.
  pgTypes.setTypeParser(114, function (val) {
    return val;
  });
});

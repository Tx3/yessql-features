"use strict";

var _ = require('lodash')
  , crypto = require('crypto')
  , Promise = require('bluebird');

var SqlModel = require('yessql-core/models/SqlModel/index')
  , classUtils = require('yessql-core/class-utils')
  , SqlModelQueryBuilder = require('yessql-core/models/SqlModel/SqlModelQueryBuilder');

/**
 * @private
 */
var hashFunc = Promise.promisify(crypto.pbkdf2, crypto);

/**
 * @private
 * @enum {String}
 */
var Role = {
  User: 'User',
  Admin: 'Admin'
};

/**
 * @private
 * @enum {Object.<Role, Number>}
 */
var RoleLevel = {
  User: 0,
  Admin: 1
};

/**
 * @extends SqlModel
 * @constructor
 */
function User () {
  SqlModel.call(this);
}

classUtils.inherits(User, SqlModel);

/**
 * @override
 */
User.prototype.$beforeValidate = function (schema, json) {
  schema = SqlModel.prototype.$beforeValidate.apply(this, arguments);
  // Password is required when creating a user.
  if (json.id === null || json.id === void 0) {
    schema.required.push('password');
  }
  return schema;
};

/**
 * @override
 */
User.prototype.$parseJson = function (json) {
  json = SqlModel.prototype.$parseJson.call(this, json);
  // Never allow passwordHash or passwordSalt to come from the outside world.
  return _.omit(json, ['passwordHash', 'passwordSalt']);
};

/**
 * @override
 */
User.prototype.$formatJson = function (json) {
  json = SqlModel.prototype.$formatJson.call(this, json);
  return _.omit(json, ['password', 'passwordHash', 'passwordSalt']);
};

/**
 * @override
 */
User.prototype.$formatDatabaseJson = function (json, shallow) {
  json = SqlModel.prototype.$formatDatabaseJson.call(this, json, shallow);
  return _.omit(json, ['password']);
};

/**
 * Asynchronously tests if a password is equal to this user's password.
 *
 * @param {String} passwordToTest
 * @return {Promise}
 */
User.prototype.$isPasswordValid = function (passwordToTest) {
  var self = this;
  var ModelClass = self.constructor;
  if (!_.isString(passwordToTest) || _.isEmpty(passwordToTest)) {
    return Promise.resolve(false);
  }
  return this
    .$fetchPasswordHashAndSalt_()
    .then(function () {
      return ModelClass.calculatePasswordHash(passwordToTest, self.passwordSalt);
    })
    .then(function (passwordHash) {
      return self.passwordHash === passwordHash;
    });
};

/**
 * Tests if the user has the given role or any role with a higher level.
 *
 * @param {Role} roleToTest
 * @return {Boolean}
 */
User.prototype.$hasRole = function (roleToTest) {
  if (!_.has(RoleLevel, roleToTest)) {
    return false;
  }
  return _.some(this.roles, function (role) {
    return role === roleToTest || RoleLevel[role] > RoleLevel[roleToTest];
  });
};

/**
 * @private
 * @return {Promise}
 */
User.prototype.$calculateAndSetPasswordHash_ = function () {
  var self = this;
  var ModelClass = this.constructor;
  return this
    .$fetchPasswordHashAndSalt_()
    .then(function () {
      return ModelClass.calculatePasswordHash(self.password, self.passwordSalt);
    })
    .then(function (passwordHash) {
      self.passwordHash = passwordHash;
      return self;
    });
};

/**
 * @private
 * @return {Promise}
 */
User.prototype.$fetchPasswordHashAndSalt_ = function () {
  if (this.id === null || this.id === void 0 || (this.passwordSalt && this.passwordHash)) {
    // No need to fetch.
    return Promise.resolve(this);
  } else {
    var self = this;
    return this.constructor
      .dbQuery()
      .where('id', self.id)
      .select('passwordSalt', 'passwordHash')
      .then(function (rows) {
        self.passwordSalt = rows[0].passwordSalt;
        self.passwordHash = rows[0].passwordHash;
        return self;
      });
  }
};

/**
 * @override
 */
User.tableName = 'User';

/**
 * @override
 */
User.schema = {
  type: 'object',
  required: ['username', 'roles'],

  properties: {
    id:        {type: ['number', 'null']},
    username:  {type: 'string', minLength: 2, maxLength: 255},
    password:  {type: 'string', minLength: 6, maxLength: 255},
    firstName: {type: ['string', 'null'], minLength: 1, maxLength: 255},
    lastName:  {type: ['string', 'null'], minLength: 1, maxLength: 255},
    email:     {oneOf: [{type: 'string', format: 'email', maxLength: 255}, {type: 'null'}]},

    passwordHash: {type: ['string', 'null']},
    passwordSalt: {type: ['string', 'null']},

    roles: {
      type: 'array',
      minItems: 1,
      items: {
        type: 'string',
        enum: _.values(Role)
      }
    }
  }
};

/**
 * @override
 */
User.jsonAttributes = ['roles'];

/**
 * Possible user roles.
 *
 * @enum {Number}
 */
User.Role = Role;

/**
 * Hierarchy levels for the user roles.
 *
 * If RoleLevel[role1] > RoleLevel[role2] the the role1 has access to the same
 * resources as user2.
 *
 * @type {Array.<Number>}
 */
User.RoleLevel = RoleLevel;

/**
 * @override
 */
User.insert = function (modelOrJson) {
  var ModelClass = this;
  var model = ModelClass.ensureModel(modelOrJson);
  var queryBuilder = SqlModelQueryBuilder.forClass(this);

  return queryBuilder
    .runBeforeExecution(function () {
      model.passwordSalt = ModelClass.createRandomSalt();
      return model.$calculateAndSetPasswordHash_();
    })
    .runBeforeExecution(function () {
      if (ModelClass.uuidRowIdentifiers) {
        model.id = ModelClass.generateId();
      }
      queryBuilder.insert(model.$toDatabaseJson()).returning('id');
    })
    .runAfterQueryExecution(function (id) {
      if (!ModelClass.uuidRowIdentifiers) {
        model.id = id[0];
      }
      return [model];
    })
    .runAfterExecution(function (models) {
      return models[0];
    });
};

/**
 * @override
 */
User.update = function (modelOrJson) {
  var model = this.ensureModel(modelOrJson);
  var queryBuilder = SqlModelQueryBuilder.forClass(this);

  return queryBuilder
    .runBeforeExecution(function () {
      if (model.password) {
        // If updating password, need to recalculate the hash.
        return model.$calculateAndSetPasswordHash_();
      }
    })
    .runBeforeExecution(function () {
      queryBuilder.update(model.$toDatabaseJson()).where('id', model.id);
    })
    .runAfterQueryExecution(function () {
      return [model];
    })
    .runAfterExecution(function (models) {
      return models[0];
    });
};

/**
 * @param {String} username
 *    Username to search for.
 *
 * @return {SqlModelQueryBuilder}
 */
User.findByUsername = function (username) {
  return this.findOne().where('username', username);
};

/**
 * Calculates password hash using the given salt.
 *
 * @param {String} password
 * @param {String} salt
 * @return {Promise}
 */
User.calculatePasswordHash = function (password, salt) {
  return hashFunc(password, salt, 10000, 256)
    .then(function (passwordHash) {
      return passwordHash.toString('hex');
    });
};

/**
 * Creates a random password salt.
 *
 * @return {String}
 */
User.createRandomSalt = function () {
  return crypto.randomBytes(128).toString('hex');
};

module.exports = User;

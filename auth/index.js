"use strict";

var _ = require('lodash')
  , Promise = require('bluebird')
  , passport = require('passport')
  , BasicStrategy = require('passport-http').BasicStrategy
  , LocalStrategy = require('passport-local')
  , classUtils = require('yessql-core/class-utils')
  , SqlModel = require('yessql-core/models/SqlModel')
  , User = require('./models/User');

/**
 * Initializes passport.js and other authentication related stuff.
 *
 * Example usage in config file:
 *
 * ```js
 * features: [
 *   ...
 *   {
 *     feature: 'auth',
 *     config: {
 *       readSessionUserFromDb: true,
 *       sessionUserEagerFetch: ['organizations']
 *       userModelClass: SomeUserModelClass
 *     }
 *   },
 *   ...
 * ]
 * ```
 *
 * @param {object} app
 *    express.js Application instance.
 *
 * @param {Object} config
 *    The configuration object.
 *
 * @param {Boolean} config.readSessionUserFromDb
 *    If `readSessionUserFromDb` is *false* the user is not read from the database for each
 *    request. Instead the cached user from the session store is used as the session user.
 *
 * @param {String|Array.<String>|Object} config.sessionUserEagerFetch
 *    Which relations to fetch with the session user. The value can be anything that
 *    `SqlModelQueryBuilder#eager` method accepts.
 *
 * @param {Function} config.userModelClass
 *    `SqlModel` subclass that has methods `findByUserName(username)` and `$isPasswordValid(password)`.
 *    `findByUsername(username)` must be a static method and return a Promise. `$isPasswordValid(password)`
 *    must be an instance method and return a Promise.
 */
module.exports = function (app, config) {
  config = config || {};
  config.userModelClass = config.userModelClass || User;

  if (!classUtils.isSubclassOf(config.userModelClass, SqlModel)) {
    throw new Error('auth: config.userModelClass must be a subclass of SqlModel');
  }

  if (!_.isFunction(config.userModelClass.findByUsername) ||
      !_.isFunction(config.userModelClass.prototype.$isPasswordValid)) {
    throw new Error('auth: config.userModelClass must have methods findByUsername and $isPasswordValid');
  }

  if (_.isUndefined(config.readSessionUserFromDb)) {
    config.readSessionUserFromDb = true;
  }

  var strategyConfig = {
    passReqToCallback: true
  };

  // This is called when a login attempt is made with username password pair.
  var verify = function (req, username, password, done) {
    if (!req.db) { throw new Error('database feature required'); }
    var user = null;
    config.userModelClass
      .bindDb(req.db)
      .findByUsername(username)
      .eager(config.sessionUserEagerFetch)
      .then(function (usr) {
        if (!usr) {
          // No user found. Delay approximately the same amount of time
          // that it takes to calculate a password hash.
          return Promise.resolve(false).delay(100);
        } else {
          user = usr;
          return usr.$isPasswordValid(password);
        }
      })
      .then(function (isPasswordValid) {
        done(null, isPasswordValid ? user : false);
      })
      .catch(function (err) {
        done(err);
      });
  };

  app.use(passport.initialize());
  app.use(passport.session());

  passport.use(new BasicStrategy(strategyConfig, verify));
  passport.use(new LocalStrategy(strategyConfig, verify));

  passport.serializeUser(function (user, done) {
    // Serialize the user to be saved in the session store.
    done(null, user.$toJson());
  });

  passport.deserializeUser(function (req, json, done) {
    if (config.readSessionUserFromDb) {
      if (!req.db) { throw new Error('database feature required if readSessionUserFromDb = true'); }
      // Load the User from the database to make sure we have the most recent version.
      config.userModelClass
        .bindDb(req.db)
        .findById(json.id)
        .eager(config.sessionUserEagerFetch)
        .then(function (user) {
          done(null, user);
        })
        .catch(function (err) {
          done(err);
        });
    } else {
      // Use the User instance saved with the session.
      done(null, config.userModelClass.fromJson(json));
    }
  });

  app.use(function (req, res, next) {
    var originalEnd = res.end;

    // Passport.js always creates an empty object in req.session.passport even
    // if nothing happens. Here we make sure it is removed.
    res.end = function () {
      // This check should blow up when passport api changes so that it no longer
      // creates the 'passport' object.
      if (!_.isObject(req.session.passport)) {
        throw new Error('passport.js API has changed.');
      }
      if (_.isEmpty(req.session.passport)) {
        delete req.session.passport;
      }
      return originalEnd.apply(res, arguments);
    };

    next();
  });
};

/**
 * These features must be initialized before this one.
 *
 * @type {Array.<String>}
 */
module.exports.dependencies = ['cookie-session|token-session'];

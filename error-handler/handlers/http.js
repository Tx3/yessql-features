"use strict";

var HTTPError = require('yessql-core/errors/HTTPError');

/**
 * Handles all `HTTPErrors`.
 */
module.exports = function(error) {
  if (error instanceof HTTPError) {
    return error.toJSON();
  }
  return null;
};
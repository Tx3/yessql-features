"use strict";

var _ = require('lodash')
  , HTTPError = require('yessql-core/errors/HTTPError')
  , AccessError = require('yessql-core/errors/AccessError')
  , Promise = require('bluebird')
  , SqlModel = require('yessql-core/models/SqlModel')
  , NotFoundError = require('yessql-core/errors/NotFoundError');

/**
 * @constructor
 */
function Route(expressRouter, method, path) {
  /**
   * @type {express.Router}
   */
  this.expressRouter = expressRouter;
  /**
   * @type {String}
   */
  this.method = method;
  /**
   * @type {String|RegExp}
   */
  this.path = path;
  /**
   * @type {Array.<function (Request, Response, function)>}
   */
  this.expressMiddleware = [];
  /**
   * @type {Array.<function (Request, Response, Transaction)>}
   */
  this.authHandlers = [];
  /**
   * @type {function (Request, Response, Transaction)}
   */
  this.handlerFunc = null;
}

/**
 * Installs an express middleware to the route.
 *
 * @param {function(IncomingMessage, Transaction, function)} middleware
 * @returns {Route}
 */
Route.prototype.middleware = function (middleware) {
  if (this.handlerFunc) {
    throw new Error('You must call middleware(func) before handler(func)');
  }

  this.expressMiddleware.push(middleware);
  return this;
};

/**
 * Installs an authentication handler for the route.
 *
 * @see Router#get for examples.
 * @param {function(IncomingMessage, Transaction)} authHandler
 * @returns {Route}
 */
Route.prototype.auth = function (authHandler) {
  if (this.handlerFunc) {
    throw new Error('You must call auth(func) before handler(func)');
  }

  this.authHandlers.push(authHandler || _.constant(true));
  return this;
};

/**
 * Installs a handler for the route.
 *
 * @see Router#get for examples.
 * @param {function(IncomingMessage, ServerResponse, Transaction)} handler
 * @returns {Route}
 */
Route.prototype.handler = function (handler) {
  if (this.handlerFunc) {
    throw new Error('handler(func) can be called just once per Route instance');
  }

  this.handlerFunc = handler;
  this.execute_();
  return this;
};

/**
 * @private
 */
Route.prototype.execute_ = function () {
  var self = this;

  this.expressRouter[this.method](this.path, function (req, res, next) {
    self.handlerMiddleware_(req, res, next);
  });
};

/**
 * @private
 */
Route.prototype.handlerMiddleware_ = function (req, res, next) {
  var self = this;
  // If the handler takes three arguments we assume that the third one is a transaction.
  // In this case we start a transaction and pass it to the handler.
  var transactionNeeded = this.handlerFunc.length === 3;
  var promise = Promise.resolve();

  if (transactionNeeded) {
    promise = SqlModel.transaction(function (trx) {
      return self.handle_(req, res, trx);
    }, req.db);
  } else {
    promise = promise.then(function () {
      return self.handle_(req, res, null);
    });
  }

  // Error handling.
  promise.catch(next);
};

/**
 * @private
 */
Route.prototype.handle_ = function (req, res, transaction) {
  var self = this;
  var context = {};
  var promise = Promise.resolve();

  _.each(self.expressMiddleware, function (middleware) {
    promise = promise.then(function () {
      return executeMiddleware(req, res, middleware);
    });
  });

  // If authentication handlers have been registered, first check that we have
  // a valid logged in user.
  if (this.authHandlers.length && !req.user) {
    throw new HTTPError(401);
  }

  _.each(self.authHandlers, function (authHandler) {
    promise = promise.then(function () {
      return authHandler.call(context, req, transaction);
    }).then(function (ret) {
      if (ret === false) {
        throw new AccessError();
      }
    });
  });

  return promise.then(function () {
    var result = self.handlerFunc.call(context, req, res, transaction);

    // If there is no return value (or the return value is undefined) assume that
    // the handler calls res.end(), res.send() or similar method explicitly.
    if (result === void 0) {
      return;
    }

    return Promise.resolve(result).then(function (result) {
      if (!result && !_.isString(result)) {
        throw new NotFoundError();
      } else {
        sendResult(result, req, res);
      }
    });
  });
};

module.exports = Route;

/**
 * @private
 */
function executeMiddleware(req, res, middleware) {
  return new Promise(function (resolve, reject) {
    var next = function (err) {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    };
    middleware(req, res, next);
  });
}

/**
 * @private
 */
function sendResult(result, req, res) {
  if (_.isObject(result)) {
    res.set('Content-Type', 'application/json');
    // Pretty print json in development and testing modes.
    if (req.app.config.profile === 'development' || req.app.config.profile === 'testing') {
      res.send(JSON.stringify(result, null, 2));
    } else {
      res.send(JSON.stringify(result));
    }
  } else {
    res.send(result);
  }
}
